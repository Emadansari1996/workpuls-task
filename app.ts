require("dotenv").config();
import { initMongo } from "./src/config/database";
import  express from 'express';
import userRouter from "./src/routes/user.route"
import officeRouter from "./src/routes/office.route";

initMongo();
const app = express();

app.use(express.json());
app.use('/users' ,userRouter );
app.use('/office', officeRouter);
// Logic goes here

export default app;