import * as http from 'http';
import app from './app';
import dotenv from 'dotenv';
const swaggerUi = require("swagger-ui-express");
const swaggerSchema = require('./swagger.json');
const server = http.createServer(app);
const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSchema));

// server listening 
server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});