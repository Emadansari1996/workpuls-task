# Workpuls-task
This project is part of selection proccess.


## Getting started

To start project you need to follow some simple steps.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Emadansari1996/workpuls-task.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:27eee0b9c74611f602474f66409b2705?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

to start project first clone files : 
```
git clone git@gitlab.com:Emadansari1996/workpuls-task.git
```

then install dependencies:
```
npm install
```
compile to make JS files:
```
npx tsc
```
run project:
```
node index.js
```

***

## Name
Workpuls selection proccess task.

## Description
In this project we have two main entities : 1.location 2.user
we used this data model to overcome requirements.
## Installation
Install dependencies : 
```
npm install
```

## Document

After running project, check this url for document : 
```
host:port/api-docs
```


