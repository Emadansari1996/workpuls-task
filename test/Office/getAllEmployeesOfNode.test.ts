import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";
import { generateToken } from "../../src/services/token.service";

const MocData = {
    "name" : "emad",
    "email": "officeEmployeeTest@gmail.com",
    "role": "employee",
    "password": "123124325",
    "location": "Severnobacki okrug",
    "token": ""
};

const descendantsEmployeeMocData = {
    "name" : "emad",
    "email": "SuboticaTest@gmail.com",
    "role": "employee",
    "password": "123124325",
    "location": "Subotica",
    "token": ""
};

const descendantsManagerMocData = {
    "name" : "emad",
    "email": "SuboticaManagerTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Subotica",
    "token": ""
};


const seedDB = async () => {
    return userMongooseSchema.insertMany([MocData,descendantsEmployeeMocData,descendantsManagerMocData]);
}

const cleanDB = async () => {
   await userMongooseSchema.deleteMany({email : {$in:[MocData.email,descendantsEmployeeMocData.email,descendantsManagerMocData.email] }});
}

beforeAll(async () => {
   const users = await seedDB();
   const MocDataToken = await generateToken(users[0]._id , users[0].email);
   const EmployeeToken = await generateToken(users[1]._id , users[1].email);
   MocData.token = MocDataToken;
   descendantsEmployeeMocData.token = EmployeeToken;
  });
  
  afterAll(() => {
   cleanDB();
  });

test("successfully getting all employees of office", async() => {

    const result = await request(app).get("/office/allEmployees/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + MocData.token);
    const expectedResponse = "{\"data\":[{\"name\":\"emad\",\"email\":\"officeEmployeeTest@gmail.com\",\"location\":\"Severnobacki okrug\",\"role\":\"employee\"}],\"message\":\"Success\"}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

test("successfully getting all employees of office and descendants", async() => {

    const result = await request(app).get("/office/allEmployeesWithDescendants/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + MocData.token);
    const expectedResponse =  "{\"data\":[{\"name\":\"emad\",\"email\":\"officeEmployeeTest@gmail.com\",\"location\":\"Severnobacki okrug\",\"role\":\"employee\"},{\"name\":\"emad\",\"email\":\"SuboticaTest@gmail.com\",\"location\":\"Subotica\",\"role\":\"employee\"}],\"message\":\"Success\"}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

test("trying to get higer location employee", async() => {

    const result = await request(app).get("/office/allEmployees/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + descendantsEmployeeMocData.token);
    const expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(403);

});