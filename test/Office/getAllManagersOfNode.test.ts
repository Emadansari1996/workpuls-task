import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";
import { generateToken } from "../../src/services/token.service";

const MocData = {
    "name" : "emad",
    "email": "officeManagerTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Severnobacki okrug",
    "token": ""
};

const descendantsEmployeeMocData = {
    "name" : "emad",
    "email": "SuboticaTestM@gmail.com",
    "role": "employee",
    "password": "123124325",
    "location": "Subotica",
    "token": ""
};

const descendantsManagerMocData = {
    "name" : "emad",
    "email": "SuboticaManagerTestM@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Subotica",
    "token": ""
};


const seedDB = async () => {
    return userMongooseSchema.insertMany([MocData,descendantsEmployeeMocData,descendantsManagerMocData]);
}

const cleanDB = async () => {
   await userMongooseSchema.deleteMany({email : {$in:[MocData.email,descendantsEmployeeMocData.email,descendantsManagerMocData.email] }});
}

beforeAll(async () => {
   const users = await seedDB();
   const MocDataToken = await generateToken(users[0]._id , users[0].email);
   const ManagerToken = await generateToken(users[2]._id , users[2].email);
   MocData.token = MocDataToken;
   descendantsManagerMocData.token = ManagerToken;
  });
  
  afterAll(() => {
   cleanDB();
  });

test("successfully getting all Managers of office", async() => {

    const result = await request(app).get("/office/allManagers/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + MocData.token);
    const expectedResponse = "{\"data\":[{\"name\":\"emad\",\"email\":\"officeManagerTest@gmail.com\",\"location\":\"Severnobacki okrug\",\"role\":\"manager\"}],\"message\":\"Success\"}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

test("successfully getting all Managers of office and descendants", async() => {

    const result = await request(app).get("/office/allManagersWithDescendants/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + MocData.token);
    const expectedResponse = "{\"data\":[{\"name\":\"emad\",\"email\":\"officeManagerTest@gmail.com\",\"location\":\"Severnobacki okrug\",\"role\":\"manager\"},{\"name\":\"emad\",\"email\":\"SuboticaManagerTestM@gmail.com\",\"location\":\"Subotica\",\"role\":\"manager\"}],\"message\":\"Success\"}";
    expect(result.statusCode).toEqual(200);

});

test("trying to get higer location manager", async() => {

    const result = await request(app).get("/office/allManagers/location/?location=Severnobacki okrug").set('Authorization', 'Bearer ' + descendantsManagerMocData.token);
    const expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(403);

});