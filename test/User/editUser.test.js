"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __importDefault(require("../../app"));
var supertest_1 = __importDefault(require("supertest"));
var User_1 = require("../../src/model/User");
var token_service_1 = require("../../src/services/token.service");
var MocData = {
    "name": "emad",
    "email": "editTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
};
var ManagerMocData = {
    "name": "josh",
    "email": "editTestManager@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
};
var EmployeeMocData = {
    "name": "jack",
    "email": "editTestEmployee@gmail.com",
    "role": "employee",
    "password": "123124325",
    "location": "Srbija",
    "token": ""
};
var MocBody = {
    "role": "employee"
};
var seedDB = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, User_1.userMongooseSchema.insertMany([MocData, EmployeeMocData, ManagerMocData])];
    });
}); };
var cleanDB = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, User_1.userMongooseSchema.deleteMany({ email: { $in: [MocData.email, EmployeeMocData.email, ManagerMocData.email] } })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
beforeAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    var users, MocDataToken, EmployeeToken, ManagerToken;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, seedDB()];
            case 1:
                users = _a.sent();
                return [4 /*yield*/, (0, token_service_1.generateToken)(users[0]._id, users[0].email)];
            case 2:
                MocDataToken = _a.sent();
                return [4 /*yield*/, (0, token_service_1.generateToken)(users[1]._id, users[1].email)];
            case 3:
                EmployeeToken = _a.sent();
                return [4 /*yield*/, (0, token_service_1.generateToken)(users[2]._id, users[2].email)];
            case 4:
                ManagerToken = _a.sent();
                MocData.token = MocDataToken;
                EmployeeMocData.token = EmployeeToken;
                ManagerMocData.token = ManagerToken;
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        cleanDB();
        return [2 /*return*/];
    });
}); });
test("successfully edit a user", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result, expectedResponse;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, (0, supertest_1.default)(app_1.default).put("/users/edit/?email=" + MocData.email).set('Authorization', 'Bearer ' + MocData.token).send(MocBody)];
            case 1:
                result = _a.sent();
                expectedResponse = "{\"data\":{\"name\":\"emad\",\"email\":\"editTest@gmail.com\",\"location\":\"Vojvodina\",\"role\":\"employee\"},\"message\":\"Success\"}";
                expect(result.text).toEqual(expectedResponse);
                expect(result.statusCode).toEqual(200);
                return [2 /*return*/];
        }
    });
}); });
test("access denied if employee edits manager", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result, expectedResponse;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, (0, supertest_1.default)(app_1.default).put("/users/edit/?email=" + ManagerMocData.email).set('Authorization', 'Bearer ' + EmployeeMocData.token).send(MocBody)];
            case 1:
                result = _a.sent();
                expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
                expect(result.text).toEqual(expectedResponse);
                expect(result.statusCode).toEqual(403);
                return [2 /*return*/];
        }
    });
}); });
test("access denied if location is higer", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result, expectedResponse;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, (0, supertest_1.default)(app_1.default).put("/users/edit/?email=" + EmployeeMocData.email).set('Authorization', 'Bearer ' + ManagerMocData.token).send(MocBody)];
            case 1:
                result = _a.sent();
                expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
                expect(result.text).toEqual(expectedResponse);
                expect(result.statusCode).toEqual(403);
                return [2 /*return*/];
        }
    });
}); });
