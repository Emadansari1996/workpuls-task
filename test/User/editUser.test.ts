import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";
import { generateToken } from "../../src/services/token.service";

const MocData = {
    "name" : "emad",
    "email": "editTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
}

const ManagerMocData = {
    "name" : "josh",
    "email": "editTestManager@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
}

const EmployeeMocData = {
    "name" : "jack",
    "email": "editTestEmployee@gmail.com",
    "role": "employee",
    "password": "123124325",
    "location": "Srbija",
    "token": ""
}

const MocBody = {
    "role": "employee"
}

const seedDB = async () => {
    return userMongooseSchema.insertMany([MocData,EmployeeMocData,ManagerMocData]);
}

const cleanDB = async () => {
   await userMongooseSchema.deleteMany({email : {$in : [MocData.email,EmployeeMocData.email,ManagerMocData.email]}});
}

beforeAll(async () => {
   const users = await seedDB();
   const MocDataToken = await generateToken(users[0]._id , users[0].email);
   const EmployeeToken = await generateToken(users[1]._id , users[1].email);
   const ManagerToken = await generateToken(users[2]._id , users[2].email);
   MocData.token = MocDataToken;
   EmployeeMocData.token = EmployeeToken;
   ManagerMocData.token = ManagerToken;
  });

afterAll(async () => {
    cleanDB();
})

test("successfully edit a user", async () => {
    
    const result = await request(app).put("/users/edit/?email="+MocData.email).set('Authorization', 'Bearer ' + MocData.token).send(MocBody);
    const expectedResponse = "{\"data\":{\"name\":\"emad\",\"email\":\"editTest@gmail.com\",\"location\":\"Vojvodina\",\"role\":\"employee\"},\"message\":\"Success\"}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

 test("access denied if employee edits manager", async () => {

    const result = await request(app).put("/users/edit/?email="+ManagerMocData.email).set('Authorization', 'Bearer ' + EmployeeMocData.token).send(MocBody);
    const expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(403); 

 });

 test("access denied if location is higer", async () => {

    const result = await request(app).put("/users/edit/?email="+EmployeeMocData.email).set('Authorization', 'Bearer ' + ManagerMocData.token).send(MocBody);
    const expectedResponse = "{\"message\":\"access denied\",\"status\":403}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(403); 

 });