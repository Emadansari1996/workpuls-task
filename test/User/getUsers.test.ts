import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";
import { generateToken } from "../../src/services/token.service";

const MocData = {
    "name" : "emad",
    "email": "test@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
}

const seedDB = async () => {
    return userMongooseSchema.create(MocData);
}

const cleanDB = async () => {
   await userMongooseSchema.deleteOne({email: MocData.email});
}

beforeAll(async () => {
   const user = await seedDB();
   const token = await generateToken(user._id , user.email);
   MocData.token = token;
  });
  
  afterAll(() => {
   cleanDB();
  });

    test("get user test success", async () => {
      const result = await request(app).get("/users/email/?email=test@gmail.com").set('Authorization', 'Bearer ' + MocData.token) ;
      const expectedResponse ="{\"data\":{\"name\":\"emad\",\"email\":\"test@gmail.com\",\"location\":\"Vojvodina\",\"role\":\"manager\"},\"message\":\"Success\"}";
      expect(result.text).toEqual(expectedResponse);
      expect(result.statusCode).toEqual(200);
    });

    test("get user test invalid token", async () => {
        const result = await request(app).get("/users/email/?email=test@gmail.com").set('Authorization', 'Bearer invalid') ;
        const expectedResponse = "{\"message\":\"Invalid Token\",\"status\":401}";
        expect(result.text).toEqual(expectedResponse);
        expect(result.statusCode).toEqual(401);
      });