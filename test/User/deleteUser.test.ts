import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";
import { generateToken } from "../../src/services/token.service";

const MocData = {
    "name" : "delete",
    "email": "deleteTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
}

const DeleterMocData = {
    "name" : "deleter",
    "email": "deleterTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
    "token": ""
}

const seedDB = async () => {
    return userMongooseSchema.insertMany([MocData,DeleterMocData]);
}

const cleanDB = async () => {
   await userMongooseSchema.deleteMany({email: {$in : [MocData.email , DeleterMocData.email]}});
}

beforeAll(async () => {
   const users = await seedDB();
   const MocDataToken = await generateToken(users[0]._id , users[0].email);
   const DeleterMocDataToken = await generateToken(users[1]._id , users[1].email);
   MocData.token = MocDataToken;
   DeleterMocData.token = DeleterMocDataToken;
  });

afterAll(async () => {
    cleanDB();
})  

test("successfully delete a user", async () => {

    const result = await request(app).delete("/users/delete/?email="+MocData.email).set('Authorization', 'Bearer ' + DeleterMocData.token) ;
    const expectedResponse ="{\"data\":true,\"message\":\"Success\"}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

test("trying to delete non-existing user", async () => {

    const result = await request(app).delete("/users/delete/?email=noexits@gmail.com").set('Authorization', 'Bearer ' + DeleterMocData.token) ;
    const expectedResponse ="{\"message\":\"access denied\",\"status\":403}";
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(403);

});