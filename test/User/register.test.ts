import app from "../../app";
import request from "supertest";
import { userMongooseSchema } from "../../src/model/User";


const MocData = {
    "name" : "emad",
    "email": "registerTest@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina"
}

const OldUserMocData = {
    "name" : "john",
    "email": "john@gmail.com",
    "role": "manager",
    "password": "123124325",
    "location": "Vojvodina",
}

const getToken = async () => {
    return userMongooseSchema.findOne({email:"registerTest@gmail.com"});
}

beforeAll(async () => {
    const user = await userMongooseSchema.create(OldUserMocData);
})

afterAll (async() => {
    await userMongooseSchema.deleteMany({email: {$in : [MocData.email , OldUserMocData.email]}});
});

test('succesfully register a user', async () => {

    const result = await await request(app).post('/users/register').send(MocData);
    const user = await getToken();
    const expectedResponse ="{\"data\":{\"email\":\"registerTest@gmail.com\",\"token\":\""+user?.token+"\"},\"message\":\"Success\"}"
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(200);

});

test('trying to use old email to register', async () => {

    const result = await await request(app).post('/users/register').send(OldUserMocData);
    const expectedResponse ="{\"message\":\"User Already exists\",\"status\":409}"
    expect(result.text).toEqual(expectedResponse);
    expect(result.statusCode).toEqual(409);

});