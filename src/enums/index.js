"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.urlEnum = exports.statusEnum = exports.roleEnum = void 0;
var roleEnum;
(function (roleEnum) {
    roleEnum["Manager"] = "manager";
    roleEnum["Employee"] = "employee";
})(roleEnum = exports.roleEnum || (exports.roleEnum = {}));
var statusEnum;
(function (statusEnum) {
    statusEnum["SUCCESS"] = "Success";
    statusEnum["FAILD"] = "Failed";
})(statusEnum = exports.statusEnum || (exports.statusEnum = {}));
var urlEnum;
(function (urlEnum) {
    urlEnum["Manager"] = "allManagers";
    urlEnum["Edit"] = "edit";
    urlEnum["Delete"] = "delete";
})(urlEnum = exports.urlEnum || (exports.urlEnum = {}));
