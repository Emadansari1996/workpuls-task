export enum roleEnum {
    Manager = "manager",
    Employee = "employee"
}

export enum statusEnum {
    SUCCESS = "Success",
    FAILD = "Failed"
}

export enum urlEnum {
    Manager = "allManagers",
    Edit = "edit",
    Delete = "delete"
}