import {registerService,updateUserService, getUser, deleteUserService} from "../services/user.service";
import { standardResponse, createError } from "../responseCreator";
import { RequestInput } from "../Interfaces";
import { validateOldUser } from "../validators/oldUserValidator";
import { getLocationEmployeesService , getLocationManagersService, getManagersOfLocationWithDescendantsService,
getEmployeesOfLocationWithDescendantsService} from "../services/location.service";


export default class Controller {
    public static async register(options :RequestInput)  {
        try{
            const { name, email, password, location, role } = options.body;
            const user = { name, email, password, location, role };
            const result = await registerService(user);
            return standardResponse({email: result.email, token: result.token} , 1);
        }
        catch(error){
            throw(error);
        }
    
    }

    public static async updateUser(options :RequestInput) {
        try{
            const currentEmail = options.query.email;
            const {name, email, password, location, role} = options.body;
            const updates = {name, email, password, location, role};

            if(email){
                // check if new email is already taken
                const is_old_user = await validateOldUser(email);
                if(is_old_user){
                    return createError('email already taken',500);
                }
            }

            //generate update object
            if(!name){
                delete updates.name;
            }
            if(!email){
                delete updates.email;
            }
            if(!password){
                delete updates.password;
            }
            if(!location){
                delete updates.location;
            }
            if(!role){
                delete updates.role;
            }

            const result = await updateUserService(updates,currentEmail);
            return standardResponse(result , 1);
        }
        catch(error){
            throw(error);
        }
    }

    public static async getUserInfo(options :RequestInput)  {

        const email = options.query.email;
        try{
            const result = await getUser(email);
            return standardResponse(result , 1);
        }
        catch(error){
            throw error;
        }

    }

    public static async deleteUser (options :RequestInput) {

        const email = options.query.email;
        
        //check if email exists
        const is_old_user = validateOldUser(email);
        if(!is_old_user){
            return createError('email is invalid', 409);
        }
        
        const result = await deleteUserService(email);
        if(result){
            return standardResponse(result , 1);
        }   
        return createError('internal error', 500);
    }

    public static async getEmployeesOfLocation (options :RequestInput) {
      
        const location = options.query.location;
        console.log(location);
        try{
            const result = await getLocationEmployeesService(location);
            return standardResponse(result , 1);
        }
        catch(error){
            throw error;
        }
    }

    public static async getManagersOfLocation (options :RequestInput) {
        const location = options.query.location;
        try{
            const result = await getLocationManagersService(location);
            return standardResponse(result , 1);
        }
        catch(error){
            throw error;
        }
    }

    public static async getEmployeesOfLocationWithDescendants (options :RequestInput) {
        const location = options.query.location;
        try{
            const result = await getEmployeesOfLocationWithDescendantsService(location);
            return standardResponse(result , 1);
        }
        catch(error){
            throw error;
        }
    }    

    public static async getManagersOfLocationWithDescendants (options :RequestInput) {
        const location = options.query.location;
        try{
            const result = await getManagersOfLocationWithDescendantsService(location);
            return standardResponse(result , 1);
        }
        catch(error){
            throw error;
        }
    }
}

