"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_service_1 = require("../services/user.service");
var responseCreator_1 = require("../responseCreator");
var oldUserValidator_1 = require("../validators/oldUserValidator");
var location_service_1 = require("../services/location.service");
var Controller = /** @class */ (function () {
    function Controller() {
    }
    Controller.register = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, name_1, email, password, location_1, role, user, result, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = options.body, name_1 = _a.name, email = _a.email, password = _a.password, location_1 = _a.location, role = _a.role;
                        user = { name: name_1, email: email, password: password, location: location_1, role: role };
                        return [4 /*yield*/, (0, user_service_1.registerService)(user)];
                    case 1:
                        result = _b.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)({ email: result.email, token: result.token }, 1)];
                    case 2:
                        error_1 = _b.sent();
                        throw (error_1);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Controller.updateUser = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var currentEmail, _a, name_2, email, password, location_2, role, updates, is_old_user, result, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 4, , 5]);
                        currentEmail = options.query.email;
                        _a = options.body, name_2 = _a.name, email = _a.email, password = _a.password, location_2 = _a.location, role = _a.role;
                        updates = { name: name_2, email: email, password: password, location: location_2, role: role };
                        if (!email) return [3 /*break*/, 2];
                        return [4 /*yield*/, (0, oldUserValidator_1.validateOldUser)(email)];
                    case 1:
                        is_old_user = _b.sent();
                        if (is_old_user) {
                            return [2 /*return*/, (0, responseCreator_1.createError)('email already taken', 500)];
                        }
                        _b.label = 2;
                    case 2:
                        //generate update object
                        if (!name_2) {
                            delete updates.name;
                        }
                        if (!email) {
                            delete updates.email;
                        }
                        if (!password) {
                            delete updates.password;
                        }
                        if (!location_2) {
                            delete updates.location;
                        }
                        if (!role) {
                            delete updates.role;
                        }
                        return [4 /*yield*/, (0, user_service_1.updateUserService)(updates, currentEmail)];
                    case 3:
                        result = _b.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 4:
                        error_2 = _b.sent();
                        throw (error_2);
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Controller.getUserInfo = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var email, result, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = options.query.email;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, (0, user_service_1.getUser)(email)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 3:
                        error_3 = _a.sent();
                        throw error_3;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Controller.deleteUser = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var email, is_old_user, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = options.query.email;
                        is_old_user = (0, oldUserValidator_1.validateOldUser)(email);
                        if (!is_old_user) {
                            return [2 /*return*/, (0, responseCreator_1.createError)('email is invalid', 409)];
                        }
                        return [4 /*yield*/, (0, user_service_1.deleteUserService)(email)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                        }
                        return [2 /*return*/, (0, responseCreator_1.createError)('internal error', 500)];
                }
            });
        });
    };
    Controller.getEmployeesOfLocation = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var location, result, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        location = options.query.location;
                        console.log(location);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, (0, location_service_1.getLocationEmployeesService)(location)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 3:
                        error_4 = _a.sent();
                        throw error_4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Controller.getManagersOfLocation = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var location, result, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        location = options.query.location;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, (0, location_service_1.getLocationManagersService)(location)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 3:
                        error_5 = _a.sent();
                        throw error_5;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Controller.getEmployeesOfLocationWithDescendants = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var location, result, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        location = options.query.location;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, (0, location_service_1.getEmployeesOfLocationWithDescendantsService)(location)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 3:
                        error_6 = _a.sent();
                        throw error_6;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Controller.getManagersOfLocationWithDescendants = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var location, result, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        location = options.query.location;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, (0, location_service_1.getManagersOfLocationWithDescendantsService)(location)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, (0, responseCreator_1.standardResponse)(result, 1)];
                    case 3:
                        error_7 = _a.sent();
                        throw error_7;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return Controller;
}());
exports.default = Controller;
