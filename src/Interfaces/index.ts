export interface UserInterface {
    name: string,
    email: string,
    location: string,
    role: string,
    password: string,
    token: string,
}

export interface LocationInterface {
    name: string,
    descendants: [string]
}

export interface RequestInput {
        body: any;
        params: any;
        query: any;
        headers: any;
}