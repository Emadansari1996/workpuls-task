"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.locationMongooseSchema = void 0;
var mongoose_1 = require("mongoose");
var locationSchema = new mongoose_1.Schema({
    name: { type: String, required: true },
    descendants: { type: [String], required: true },
});
exports.locationMongooseSchema = (0, mongoose_1.model)('location', locationSchema);
