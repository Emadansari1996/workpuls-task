"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userMongooseSchema = void 0;
var mongoose = require("mongoose");
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    name: { type: String, default: null },
    email: { type: String, required: true },
    location: { type: String, required: true },
    role: { type: String, required: true },
    password: { type: String },
    token: { type: String },
});
exports.userMongooseSchema = (0, mongoose_1.model)('user', userSchema);
