import {LocationInterface} from '../Interfaces';
import {Document, model, Schema} from "mongoose"

interface LocationMongooseDocument extends LocationInterface, Document {
}
const locationSchema = new Schema({
    name: { type: String, required: true },
    descendants: {type: [String], required: true},
})

export const locationMongooseSchema = model<LocationMongooseDocument>('location', locationSchema)