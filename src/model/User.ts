const mongoose = require("mongoose");
import {UserInterface} from '../Interfaces';
import {Document, model, Schema} from "mongoose"

interface UserMongooseDocument extends UserInterface, Document {
}

const userSchema = new Schema(
  {
    name: { type: String, default: null },
    email: { type: String, required: true },
    location: { type: String, required: true },
    role: {type: String, required:true},
    password: { type: String },
    token: { type: String },
  }
)

export const userMongooseSchema = model<UserMongooseDocument>('user', userSchema)

