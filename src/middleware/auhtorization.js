"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAccess = void 0;
var userRepo_1 = require("../Repositories/userRepo");
var locationRepo_1 = require("../Repositories/locationRepo");
var enums_1 = require("../enums");
var responseCreator_1 = require("../responseCreator");
var checkUserAccess = function (reqRole, targetRole) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (reqRole === enums_1.roleEnum.Manager) {
            return [2 /*return*/, true];
        }
        if (targetRole === enums_1.roleEnum.Employee) {
            return [2 /*return*/, true];
        }
        return [2 /*return*/, false];
    });
}); };
var checkUserLocationAccess = function (reqLoc, targetLoc) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (reqLoc.name === targetLoc) {
            return [2 /*return*/, true];
        }
        if (reqLoc.descendants.includes(targetLoc)) {
            return [2 /*return*/, true];
        }
        return [2 /*return*/, false];
    });
}); };
var checkAccessForUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, targetUser, userRoleHasAccess, standardError, userLocation, userLocHasAccess, standardError;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, (0, userRepo_1.UserfindOneByEmail)(req.user.email)];
            case 1:
                user = _a.sent();
                return [4 /*yield*/, (0, userRepo_1.UserfindOneByEmail)(req.query.email)];
            case 2:
                targetUser = _a.sent();
                return [4 /*yield*/, checkUserAccess(user === null || user === void 0 ? void 0 : user.role, targetUser === null || targetUser === void 0 ? void 0 : targetUser.role)];
            case 3:
                userRoleHasAccess = _a.sent();
                if (!!userRoleHasAccess) return [3 /*break*/, 5];
                return [4 /*yield*/, (0, responseCreator_1.createError)('access denied', 403)];
            case 4:
                standardError = _a.sent();
                return [2 /*return*/, res.status(403).send(standardError)];
            case 5: return [4 /*yield*/, (0, locationRepo_1.findLocationByName)((user === null || user === void 0 ? void 0 : user.location) || '')];
            case 6:
                userLocation = _a.sent();
                return [4 /*yield*/, checkUserLocationAccess(userLocation, targetUser === null || targetUser === void 0 ? void 0 : targetUser.location)];
            case 7:
                userLocHasAccess = _a.sent();
                if (!!userLocHasAccess) return [3 /*break*/, 9];
                return [4 /*yield*/, (0, responseCreator_1.createError)('access denied', 403)];
            case 8:
                standardError = _a.sent();
                return [2 /*return*/, res.status(403).send(standardError)];
            case 9: return [2 /*return*/, next()];
        }
    });
}); };
var checkAccessForOffice = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var location, user, standardError, userLocation, is_location_valid, standardError;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                location = req.query.location;
                return [4 /*yield*/, (0, userRepo_1.UserfindOneByEmail)(req.user.email)];
            case 1:
                user = _a.sent();
                if (!(req.url.toString().includes(enums_1.urlEnum.Manager) && (user === null || user === void 0 ? void 0 : user.role) === enums_1.roleEnum.Employee)) return [3 /*break*/, 3];
                return [4 /*yield*/, (0, responseCreator_1.createError)('access denied', 403)];
            case 2:
                standardError = _a.sent();
                return [2 /*return*/, res.status(403).send(standardError)];
            case 3: return [4 /*yield*/, (0, locationRepo_1.findLocationByName)((user === null || user === void 0 ? void 0 : user.location) || '')];
            case 4:
                userLocation = _a.sent();
                return [4 /*yield*/, checkUserLocationAccess(userLocation || '', location)];
            case 5:
                is_location_valid = _a.sent();
                if (!!is_location_valid) return [3 /*break*/, 7];
                return [4 /*yield*/, (0, responseCreator_1.createError)('access denied', 403)];
            case 6:
                standardError = _a.sent();
                return [2 /*return*/, res.status(403).send(standardError)];
            case 7: return [2 /*return*/, next()];
        }
    });
}); };
var checkAccess = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (req.query.email) {
            checkAccessForUser(req, res, next);
        }
        if (req.query.location) {
            checkAccessForOffice(req, res, next);
        }
        return [2 /*return*/];
    });
}); };
exports.checkAccess = checkAccess;
