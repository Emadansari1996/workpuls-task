import { UserfindOneByEmail } from '../Repositories/userRepo';
import { findLocationByName } from '../Repositories/locationRepo';
import { roleEnum, urlEnum } from '../enums';
import { createError } from '../responseCreator'; 

const checkUserAccess = async (reqRole :any ,targetRole :any) => {
    if(reqRole === roleEnum.Manager){
        return true;
    }
    if(targetRole === roleEnum.Employee){
        return true;
    }
    return false;
}

const checkUserLocationAccess = async (reqLoc :any , targetLoc :any) => {
    if(reqLoc.name === targetLoc){
        return true;
    }
    if(reqLoc.descendants.includes(targetLoc)){
        return true;
    }
    return false;
}


const checkAccessForUser = async (req: any, res: any, next: any) => {

    const user = await UserfindOneByEmail(req.user.email);
    const targetUser = await UserfindOneByEmail(req.query.email);
    const userRoleHasAccess = await checkUserAccess(user?.role,targetUser?.role)

    if(!userRoleHasAccess){
        const standardError = await createError('access denied',403);
        return res.status(403).send(standardError);
    }

    const userLocation = await findLocationByName(user?.location || '');
    const userLocHasAccess = await checkUserLocationAccess(userLocation,targetUser?.location);

    if(!userLocHasAccess){
        const standardError = await createError('access denied',403);
        return res.status(403).send(standardError);
    }
    return next();
}


const checkAccessForOffice = async (req :any, res :any, next: any) => {

    const location = req.query.location;
    const user = await UserfindOneByEmail(req.user.email);

    if(req.url.toString().includes(urlEnum.Manager) && user?.role === roleEnum.Employee){
        const standardError = await createError('access denied',403);
        return res.status(403).send(standardError);
    }
    const userLocation = await findLocationByName(user?.location || '');
    const is_location_valid = await checkUserLocationAccess(userLocation || '',location);
    if(!is_location_valid){
        const standardError = await createError('access denied',403);
        return res.status(403).send(standardError);
    }
    return next();
}

export const checkAccess = async(req :any ,res :any ,next :any) => {
    if(req.query.email){
        checkAccessForUser(req,res,next);
    }
    if(req.query.location){
        checkAccessForOffice(req,res,next);
    }

}

