import {UserfindOneByEmail} from '../Repositories/userRepo';
import * as bcrypt from "bcrypt";
import  {sign,verify} from "jsonwebtoken";
import { createError , standardResponse } from "../responseCreator";

export const loginAth = async(req :any,res: any) => {
    try{
        const  {email,password} = req.body ;
        const user = await UserfindOneByEmail(email);
      
        if (user && (await bcrypt.compare(password, user.password))) {
          // Create token
          const token = sign(
            { user_id: user._id, email },
            process.env.TOKEN_KEY || '',
            {
              expiresIn: process.env.TOKEN_EXPIRE_TIME,
            }
          );
      
          // save user token
          user.token = token;
      
          // user
          const response = await standardResponse({email: user.email, token: user.token}, 1);
          res.status(200).json(response);
        }
        else{
            const standardError = await createError("Invalid Credentials",400);
            res.status(400).send(standardError);  
        }
    } 
    catch(error){
        throw(error);
    } 
}

export const authenticateRequest = async(req :any,res :any,next :any) => {
  if (!req.headers.authorization) {
    const standardError = await createError("A token is required for authentication",403);
    return res.status(403).send(standardError);
}
const parts = req.headers.authorization.split(' ');
if (parts.length !== 2) {
  const standardError = await createError("A token is required for authentication",403);
  return res.status(403).send(standardError);
}
  const token = parts[1];
  //console.log(token);
  try {
    const decoded = verify(token, process.env.TOKEN_KEY || '');
    req.user = decoded;
  } catch (err) {
    console.log(err);
    const standardError = await createError("Invalid Token",401);
    return res.status(401).send(standardError);
  }
   return next();
}