import { userMongooseSchema } from "../model/User";
import { roleEnum } from "../enums";

const select_object = {
    name: 1,
    email: 1,
    role: 1,
    location: 1,
    _id: 0
};

export const UserfindOneByEmail = async (email :string) => {
    return userMongooseSchema.findOne({email:email});
}

export const createNewUser = async (user :string) => {
    return userMongooseSchema.create(user);
}

export const saveUser = async (user :any) => {
    return user.save();
}

export const updateUser = async (updates: any , email :string) => {
    return userMongooseSchema.findOneAndUpdate({email:email}, updates ,  {new: true}).select(select_object);
}

export const getUserByEmail = async (email :string) => {
    return userMongooseSchema.findOne({email: email}).select(select_object);
}

export const getLocationEmployees = async (location :string) => {
    return userMongooseSchema.find({location : location , role : roleEnum.Employee }).select(select_object);
}

export const getLocationManagers = async (location :string) => {
    return userMongooseSchema.find({location : location , role : roleEnum.Manager }).select(select_object);
}

export const getLocationsEmployees = async (locations :any) => {
    return userMongooseSchema.find({location : {$in : locations}, role : roleEnum.Employee}).select(select_object);
}

export const getLocationsManagers = async (locations :any) => {
    return userMongooseSchema.find({location : {$in : locations}, role : roleEnum.Manager}).select(select_object);
}

export const deleteUser = async (email :string) => {
    return await userMongooseSchema.deleteOne({email : email});
}