import {locationMongooseSchema} from '../model/Location';

export const findLocationByName = async (name :string) => {
    return locationMongooseSchema.findOne({name: name});
}
