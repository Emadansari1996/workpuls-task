import express from 'express'; 
const userRouter = express.Router();
import {validateRegister} from '../validators/registerValidator';
import {loginValidator} from '../validators/loginValidator';
import {validateUpdateUser} from '../validators/validateUpdateUser';
import {loginAth,authenticateRequest} from '../middleware/authentication';
import { getUserValidator } from '../validators/getUserValidator';
import { deleteUserValidator } from '../validators/deleteUserValidator';
import Controller from '../cotrollers';
import {checkAccess} from '../middleware/auhtorization';

userRouter.get('/:email' , [authenticateRequest,getUserValidator , checkAccess] , async(req :any,res :any) => {
    const result = await Controller.getUserInfo({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
});

userRouter.post('/register',validateRegister, async(req,res) => {
    const result = await Controller.register({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

userRouter.post('/login', loginValidator, loginAth);

userRouter.put('/edit', [authenticateRequest,validateUpdateUser,checkAccess], async(req :any,res :any) => {
    const result = await Controller.updateUser({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

userRouter.delete('/delete',[authenticateRequest,deleteUserValidator,checkAccess], async(req :any , res :any) => {
    const result = await Controller.deleteUser({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

export default userRouter;