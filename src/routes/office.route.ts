import express from 'express';
import { authenticateRequest } from '../middleware/authentication';
import { officeLocationValidator } from '../validators/officeValidator';
import { checkAccess } from '../middleware/auhtorization';
import Controller from '../cotrollers';

const officeRouter = express.Router();


officeRouter.get('/allEmployees/:location',[authenticateRequest,officeLocationValidator,checkAccess] ,async(req :any , res :any) => {
    const result = await Controller.getEmployeesOfLocation({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

officeRouter.get('/allEmployeesWithDescendants/:location',[authenticateRequest,officeLocationValidator,checkAccess], async(req :any,res :any) => {
    const result = await Controller.getEmployeesOfLocationWithDescendants({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

officeRouter.get('/allManagers/:location',[authenticateRequest,officeLocationValidator,checkAccess],async(req :any,res :any) => {
    const result = await Controller.getManagersOfLocation({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

officeRouter.get('/allManagersWithDescendants/:location',[authenticateRequest,officeLocationValidator,checkAccess],async(req :any,res :any) => {
    const result = await Controller.getManagersOfLocationWithDescendants({
        body: req.body,
        params: req.params,
        query: req.query,
        headers: req.headers
    });
    res.status(200).send(result);
})

export default officeRouter;