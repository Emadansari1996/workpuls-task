import { findLocationByName } from "../Repositories/locationRepo"
export const locationValidator = async (name :string) => {
    const location = await findLocationByName(name);
    if(location){
        return true;
    } 
    return false;
}