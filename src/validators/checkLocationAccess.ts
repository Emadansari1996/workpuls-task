import { findLocationByName } from "../Repositories/locationRepo"
import { UserfindOneByEmail } from "../Repositories/userRepo"

export const checkLocationAccess = async (location :string , email :string) => {
    const user = await UserfindOneByEmail(email);
    const userLocationInfo = await findLocationByName(user?.location || '');
    if(!userLocationInfo){
        return false;
    }
    const userAllLocations = userLocationInfo.descendants;
    userAllLocations.push(user?.location || '');
    if(userAllLocations.includes(location)){
        return true;
    }
    return false;
}