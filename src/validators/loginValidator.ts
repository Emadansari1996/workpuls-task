import { createError } from "../responseCreator";

export const loginValidator = async (req :any,res :any,next :any) => {
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      const standardError = await createError("All input is required",400)
      res.status(400).send(standardError);
    }
    return next();
}

