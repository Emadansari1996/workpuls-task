import { validateOldUser } from "./oldUserValidator"
import { createError } from "../responseCreator";

export const getUserValidator = async (req :any, res :any, next :any) => {

    const email = req.query.email;
    if(!email){
        const error = await createError('email muse be sent',409);
        return res.status(409).send(error);
    }

    const is_user = await validateOldUser(email);
    if(!is_user){
        const error = await createError('user not found', 404);
        return res.status(404).send(error);
    }

    return next();

}