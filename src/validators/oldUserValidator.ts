import {UserfindOneByEmail} from '../Repositories/userRepo';

export const validateOldUser = async (email :string) => {
    const user = await UserfindOneByEmail(email);
    if(user){
        return true;
    }
    return false;
}