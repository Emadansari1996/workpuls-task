import { locationValidator } from "./locationValidator"
import { createError } from "../responseCreator";

export const officeLocationValidator = async (req :any , res: any , next :any) => {
    const location = req.query.location;
    if(!location){
        const standardError = await createError('invalid location',409);
        return res.status(409).send(standardError);
    }

    const is_location_valid = await locationValidator(location);
    if(!is_location_valid){
        const standardError = await createError('invalid location',409);
        return res.status(409).send(standardError);
    }

    return next();
}
