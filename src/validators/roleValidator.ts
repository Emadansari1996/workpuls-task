import { roleEnum } from "../enums"

export const roleValidator = async (role :string) => {

    if([roleEnum.Manager.toString(),roleEnum.Employee.toString()].includes(role)){
        return true;
    }
    return false;
}