import { createError } from "../responseCreator";
import { validateOldUser } from "./oldUserValidator";

export const deleteUserValidator = async (req :any , res: any ,next: any) => {

    const email = req.query.email;

    if(!email){
        const standardError = await createError('email is required', 409);
        return res.status(409).send(standardError);
    }

    return next();
}