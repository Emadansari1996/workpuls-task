import { createError, standardResponse } from "../responseCreator";
import { validateOldUser } from "./oldUserValidator";
import { locationValidator } from "./locationValidator";
import { roleValidator } from "./roleValidator";

export const validateUpdateUser = async (req :any,res :any,next :any) => {

   const {name,location,role,email,password} = req.body;
   const currentEmail = req.query.email;

   if(!currentEmail){
      const standardError = await createError('no user selected',400);
      return res.status(400).send(standardError);
   }

   if(!(name || location || role || email || password )){
        const standardError = await createError('no field is sent',400);
        return res.status(400).send(standardError);
   }

   //check if target user is valid
   const user = await validateOldUser(currentEmail);
   if(!user){
      const standardError = await createError('target user is invalid',400);
      return res.status(400).send(standardError);
   }

   //check if location is valid if location included
   if(location){
      const is_location_valid = await locationValidator(location);
      if(!is_location_valid){
         const standardError = await createError('location is invalid',400);
         return res.status(400).send(standardError);
      }
   }

   //check if role valid
   if(role){
      const is_role_valid = await roleValidator(role);
      if(!is_role_valid){
         const standardError = await createError('role is invalid', 400);
         return res.status(400).send(standardError);
      }
   }

   return next();
}

