import { validateOldUser } from './oldUserValidator';
import { createError } from '../responseCreator';
import { locationValidator } from './locationValidator';
import { roleValidator } from './roleValidator';

export const validateRegister = async(req :any,res :any,next :any) => {

    const { name, email, password, location, role } = req.body;
    if (!(email && password && name && location && role)) {
        res.status(400).send("All input is required");
      }

    // checkking if user exists  
    const oldUser = await validateOldUser(email);
    if (oldUser) {
        const standardError = await createError('User Already exists', 409);
        return res.status(409).send(standardError);
      }

    // check if location is valid  
    const is_valid_location = await locationValidator(location);
    if(!is_valid_location){
      const standardError = await createError('invalid location', 409);
      return res.status(409).send(standardError);
    }
    
    //check if role is valid
    const is_role_valid = await roleValidator(role);
    if(!is_role_valid){
      const standardError = await createError('invalid role', 409);
      return res.status(409).send(standardError);
    }

     return next();
}

