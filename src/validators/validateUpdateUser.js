"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateUpdateUser = void 0;
var responseCreator_1 = require("../responseCreator");
var oldUserValidator_1 = require("./oldUserValidator");
var locationValidator_1 = require("./locationValidator");
var roleValidator_1 = require("./roleValidator");
var validateUpdateUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, name, location, role, email, password, currentEmail, standardError, standardError, user, standardError, is_location_valid, standardError, is_role_valid, standardError;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, name = _a.name, location = _a.location, role = _a.role, email = _a.email, password = _a.password;
                currentEmail = req.query.email;
                if (!!currentEmail) return [3 /*break*/, 2];
                return [4 /*yield*/, (0, responseCreator_1.createError)('no user selected', 400)];
            case 1:
                standardError = _b.sent();
                return [2 /*return*/, res.status(400).send(standardError)];
            case 2:
                if (!!(name || location || role || email || password)) return [3 /*break*/, 4];
                return [4 /*yield*/, (0, responseCreator_1.createError)('no field is sent', 400)];
            case 3:
                standardError = _b.sent();
                return [2 /*return*/, res.status(400).send(standardError)];
            case 4: return [4 /*yield*/, (0, oldUserValidator_1.validateOldUser)(currentEmail)];
            case 5:
                user = _b.sent();
                if (!!user) return [3 /*break*/, 7];
                return [4 /*yield*/, (0, responseCreator_1.createError)('target user is invalid', 400)];
            case 6:
                standardError = _b.sent();
                return [2 /*return*/, res.status(400).send(standardError)];
            case 7:
                if (!location) return [3 /*break*/, 10];
                return [4 /*yield*/, (0, locationValidator_1.locationValidator)(location)];
            case 8:
                is_location_valid = _b.sent();
                if (!!is_location_valid) return [3 /*break*/, 10];
                return [4 /*yield*/, (0, responseCreator_1.createError)('location is invalid', 400)];
            case 9:
                standardError = _b.sent();
                return [2 /*return*/, res.status(400).send(standardError)];
            case 10:
                if (!role) return [3 /*break*/, 13];
                return [4 /*yield*/, (0, roleValidator_1.roleValidator)(role)];
            case 11:
                is_role_valid = _b.sent();
                if (!!is_role_valid) return [3 /*break*/, 13];
                return [4 /*yield*/, (0, responseCreator_1.createError)('role is invalid', 400)];
            case 12:
                standardError = _b.sent();
                return [2 /*return*/, res.status(400).send(standardError)];
            case 13: return [2 /*return*/, next()];
        }
    });
}); };
exports.validateUpdateUser = validateUpdateUser;
