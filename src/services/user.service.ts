import {createNewUser,saveUser,updateUser,getUserByEmail,deleteUser} from '../Repositories/userRepo';
import * as bcrypt from 'bcrypt';
import { generateToken } from './token.service';

export const registerService = async(userData :any) => {
    const encryptedPassword = await bcrypt.hash(userData.password, 10);
    userData.password = encryptedPassword;
    const user = await createNewUser(userData);
    const token = await generateToken(user._id,userData.email);
    user.token = token;
    return saveUser(user);
}

export const updateUserService = async(updates :any, email :string) => {
    if(updates.password){
        const encryptedPassword = await bcrypt.hash(updates.password, 10);
        updates.password = encryptedPassword;
    }
   return updateUser(updates , email);  
}

export const getUser = async(email :string) => {
    return getUserByEmail(email);
}

export const deleteUserService = async (email :string) => {
    const result = await deleteUser(email);
    if(result.deletedCount){
        return true;
    }
    return false;
}