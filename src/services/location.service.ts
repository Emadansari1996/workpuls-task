import { getLocationEmployees, getLocationManagers ,getLocationsManagers , getLocationsEmployees } from "../Repositories/userRepo";
import { findLocationByName } from "../Repositories/locationRepo";

export const getLocationEmployeesService = async (location :string) => {
    return getLocationEmployees(location);
}

export const getLocationManagersService = async (location :string) => {
    return getLocationManagers(location);
}

export const getEmployeesOfLocationWithDescendantsService = async (location :string) => {
    const locationInfo = await findLocationByName(location);
    const allLocations = locationInfo?.descendants;
    allLocations?.push(location);
    return getLocationsEmployees(allLocations);
}

export const getManagersOfLocationWithDescendantsService = async (location :string) => {
    const locationInfo = await findLocationByName(location);
    const allLocations = locationInfo?.descendants;
    allLocations?.push(location);
    return getLocationsManagers(allLocations);
}