import {sign} from 'jsonwebtoken';

export const generateToken = async(userId :string , email :string) => {
    return sign(
        { user_id: userId, email },
        process.env.TOKEN_KEY || '',
        {
          expiresIn: process.env.TOKEN_EXPIRE_TIME,
        }
      );
}