import { statusEnum } from "../enums";

export const standardResponse = async(result :any , status: number) => {
    const status_message = status ? statusEnum.SUCCESS : statusEnum.FAILD ;
    const response = {data : result , message : status_message};
    return response;
}

export const createError = async(message :string , status :number) => {
    return {message: message, status:status};
}