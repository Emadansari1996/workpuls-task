const  MONGO_URL = process.env.MONGO_URL?.toString();
const BASE_URL = MONGO_URL + "/" + process.env.MONGO_DB_NAME?.toString();
console.log(BASE_URL);
import {connect, connection} from "mongoose";


export const initMongo = async () => {
    await connect(BASE_URL,{});
    const db = connection;
    db.on('error', (err) => {
        console.log('db connection error... ', err);
        throw  err
    });
    db.once('open', () => {
        console.log('db opened...');
    });
};

