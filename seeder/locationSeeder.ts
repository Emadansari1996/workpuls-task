import { locationMongooseSchema } from "../src/model/Location";
const BASE_URL = "mongodb://localhost:27017/workpuls"
console.log(BASE_URL);
import {connect , connection} from "mongoose";

     connect(BASE_URL,{}).then(() => {
         console.log("connect to mongo to seed");
     })
     .catch((error) => {
         throw(error);
     })


const locations = [
    {
        name: "Srbija",
        descendants: ['Vojvodina','Severnobacki okrug','Juznobacki okrug', 'Subotica','Novi Sad', 'Radnja 1','Detelinara','Liman','Radnja 2','Radnja 3','Radnja 4', 'Radnja 5',
                      'Grad Beograd', 'Novi Beograd', 'Vracar', 'Bezanija','Radnja 6','Neimar','Radnja 7','Crveni krst','Radnja 8','Radnja 9']
    },
    {
        name: "Vojvodina",
        descendants: ['Severnobacki okrug','Juznobacki okrug', 'Subotica','Novi Sad', 'Radnja 1','Detelinara','Liman','Radnja 2','Radnja 3','Radnja 4', 'Radnja 5']
    },
    {
        name: "Grad Beograd",
        descendants: ['Novi Beograd', 'Vracar', 'Bezanija','Radnja 6','Neimar','Radnja 7','Crveni krst','Radnja 8','Radnja 9']
    },
    {
        name: "Severnobacki okrug",
        descendants: ['Subotica','Radnja 1']
    },
    {
        name: "Juznobacki okrug",
        descendants: ['Novi Sad','Detelinara','Liman','Radnja 2','Radnja 3','Radnja 4', 'Radnja 5']
    },
    {
        name: "Novi Beograd",
        descendants: ['Bezanija','Radnja 6']
    },
    {
        name: "Vracar",
        descendants: ['Neimar','Radnja 7','Crveni krst','Radnja 8','Radnja 9']
    },
    {
        name: "Subotica",
        descendants: ["Radnja 1"]
    },
    {
        name: "Novi Sad",
        descendants: ['Detelinara','Liman','Radnja 2','Radnja 3','Radnja 4', 'Radnja 5']
    },
    {
        name:"Bezanija",
        descendants: ['Radnja 6']
    },
    {
        name: "Neimar",
        descendants: ['Radnja 7']
    },
    {
        name: "Crveni krst",
        descendants: ['Radnja 8','Radnja 9']
    },
    {
        name: "Radnja 1",
        descendants: []
    },
    {
        name: "Detelinara",
        descendants: ['Radnja 2','Radnja 3']
    },
    {
        name: "Liman",
        descendants: ['Radnja 4', 'Radnja 5']
    },
    {
        name: "Radnja 6",
        descendants: []
    },
    {
        name: "Radnja 7",
        descendants: []
    },
    {
        name: "Radnja 8",
        descendants: []
    },
    {
        name: "Radnja 9",
        descendants: []
    },
    {
        name: "Radnja 2",
        descendants: []
    },
    {
        name: "Radnja 3",
        descendants: []
    },
    {
        name: "Radnja 4",
        descendants: []
    },
    {
        name: "Radnja 5",
        descendants: []
    },
]

const seedDb = async () => {
    await locationMongooseSchema.deleteMany({});
    await locationMongooseSchema.insertMany(locations);
}

seedDb().then(() => {
    connection.close();
})