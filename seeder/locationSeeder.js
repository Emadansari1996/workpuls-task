"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Location_1 = require("../src/model/Location");
var BASE_URL = "mongodb://localhost:27017/workpuls";
console.log(BASE_URL);
var mongoose_1 = require("mongoose");
(0, mongoose_1.connect)(BASE_URL, {}).then(function () {
    console.log("connect to mongo to seed");
})
    .catch(function (error) {
    throw (error);
});
var locations = [
    {
        name: "Srbija",
        descendants: ['Vojvodina', 'Severnobacki okrug', 'Juznobacki okrug', 'Subotica', 'Novi Sad', 'Radnja 1', 'Detelinara', 'Liman', 'Radnja 2', 'Radnja 3', 'Radnja 4', 'Radnja 5',
            'Grad Beograd', 'Novi Beograd', 'Vracar', 'Bezanija', 'Radnja 6', 'Neimar', 'Radnja 7', 'Crveni krst', 'Radnja 8', 'Radnja 9']
    },
    {
        name: "Vojvodina",
        descendants: ['Severnobacki okrug', 'Juznobacki okrug', 'Subotica', 'Novi Sad', 'Radnja 1', 'Detelinara', 'Liman', 'Radnja 2', 'Radnja 3', 'Radnja 4', 'Radnja 5']
    },
    {
        name: "Grad Beograd",
        descendants: ['Novi Beograd', 'Vracar', 'Bezanija', 'Radnja 6', 'Neimar', 'Radnja 7', 'Crveni krst', 'Radnja 8', 'Radnja 9']
    },
    {
        name: "Severnobacki okrug",
        descendants: ['Subotica', 'Radnja 1']
    },
    {
        name: "Juznobacki okrug",
        descendants: ['Novi Sad', 'Detelinara', 'Liman', 'Radnja 2', 'Radnja 3', 'Radnja 4', 'Radnja 5']
    },
    {
        name: "Novi Beograd",
        descendants: ['Bezanija', 'Radnja 6']
    },
    {
        name: "Vracar",
        descendants: ['Neimar', 'Radnja 7', 'Crveni krst', 'Radnja 8', 'Radnja 9']
    },
    {
        name: "Subotica",
        descendants: ["Radnja 1"]
    },
    {
        name: "Novi Sad",
        descendants: ['Detelinara', 'Liman', 'Radnja 2', 'Radnja 3', 'Radnja 4', 'Radnja 5']
    },
    {
        name: "Bezanija",
        descendants: ['Radnja 6']
    },
    {
        name: "Neimar",
        descendants: ['Radnja 7']
    },
    {
        name: "Crveni krst",
        descendants: ['Radnja 8', 'Radnja 9']
    },
    {
        name: "Radnja 1",
        descendants: []
    },
    {
        name: "Detelinara",
        descendants: ['Radnja 2', 'Radnja 3']
    },
    {
        name: "Liman",
        descendants: ['Radnja 4', 'Radnja 5']
    },
    {
        name: "Radnja 6",
        descendants: []
    },
    {
        name: "Radnja 7",
        descendants: []
    },
    {
        name: "Radnja 8",
        descendants: []
    },
    {
        name: "Radnja 9",
        descendants: []
    },
    {
        name: "Radnja 2",
        descendants: []
    },
    {
        name: "Radnja 3",
        descendants: []
    },
    {
        name: "Radnja 4",
        descendants: []
    },
    {
        name: "Radnja 5",
        descendants: []
    },
];
var seedDb = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Location_1.locationMongooseSchema.deleteMany({})];
            case 1:
                _a.sent();
                return [4 /*yield*/, Location_1.locationMongooseSchema.insertMany(locations)];
            case 2:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
seedDb().then(function () {
    mongoose_1.connection.close();
});
